#!/usr/bin/env node

const program = require('commander');
const fs = require('node-fs');
const _ = require('lodash');

program
  .arguments('<domain_name>')
  .option('-d, --folder <folder>', 'Folder to generate domain')
  .action(function (domainName) {

    const path = function (p) {
      if (p) {
        return p + '/';
      }
      return '';
    }(program.folder) + domainName + '/';
    const folders = [ 'actions', 'components', 'reducers', 'constants' ];

    folders.forEach(function (f) {
      fs.mkdirSync(path + f, 0777, true);
      switch (f) {
        case 'actions':
          writeFile(path + f, domainName, 'Action')
          break;
        case 'components':
          writeFile(path + f, domainName, 'App')
          break;
        case 'reducers':
          writeFile(path + f, domainName, 'Reducer')
          break;
        case 'constants':
          writeFile(path + f, domainName, 'Constants')
          break;
        default:
          break;
      }
    });
  })
  .parse(process.argv);

function writeFile(dirPath, domainName, templateName) {
  const filePath = dirPath + '/' + _.capitalize(domainName) + templateName + '.js';
  const lines = fs.readFileSync(__dirname + '/templates/' + templateName + '.js', 'utf-8');
  fs.writeFileSync(filePath, lines.replace(/%name%/g, _.capitalize(domainName)));
  console.log(filePath + ' created');
}