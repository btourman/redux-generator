import React, { Component } from 'react'
import { connect } from 'react-redux'

class %name%App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }

    render() {
        return (
            <div>
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        test: store.test
    }
}

%name%App.propTypes = {
    children: React.PropTypes.element
}

%name%App.defaultProps = {
    test: ''
}

export default connect(mapStateToProps)(%name%App)