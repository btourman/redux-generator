import fetch from 'isomorphic-fetch'

const %name%Action = {
    test() {
        return (dispatch) => {
            return fetch('http://127.0.0.1:8000', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ test: 'test' })
            }).then(response => console.log(response.status))
        }
    }
}

export default %name%Action
